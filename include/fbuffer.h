#ifndef FBUFFER_H
#define FBUFFER_H

#ifndef PRINTF_IMPL_INCLUDED
#error "You shouldn't be including this file directly"
#endif

#include <algorithm>
#include <string>
#include <ostream>
#include <cstdio>

#include <unistd.h>

#ifndef FORMAT_BUFFER_ALLOC_SIZE
#define FORMAT_BUFFER_ALLOC_SIZE 4096
#endif

namespace superfancy {
	template <typename Char, size_t buffer_size = FORMAT_BUFFER_ALLOC_SIZE>
	class FormatBuffer {
	public:
		using char_type = Char;
		using char_traits = std::char_traits<char_type>;
		using allocator_type = std::allocator<char_type>;
		using string_type = std::basic_string<char_type, char_traits, allocator_type>;
		using stream_type = std::basic_ostream<char_type, char_traits>;

	private:
		size_t    used_;
		char_type buff_[buffer_size];

		virtual bool write_out(const Char *base, size_t size) = 0;

	public:
		FormatBuffer() : used_(0) { }
		virtual ~FormatBuffer() { }

		bool flush() {
			if (used_) {
				if (!write_out(buff_, used_)) return false;
				used_ = 0;
			}
			return true;
		}

		bool write(const char_type *base, size_t size) {
			if (used_ + size > sizeof(buff_)) {
				if (used_) {
					size_t rem = sizeof(buff_) - used_;
					if (rem) {
						std::copy(base, base + rem, buff_ + used_);
						base += rem;
						size -= rem;
						used_ += rem;
					}
					if (!flush()) return false;
				}

				if (size > sizeof(buff_)) {
					size_t rem = size % sizeof(buff_);
					if (!write_out(base, size - rem)) return false;
					base += size - rem;
					size  = rem;
				}
			}

			std::copy(base, base + size, buff_ + used_);
			used_ += size;

			return true;
		}

		bool write(char_type ch) {
			if (used_ >= sizeof(buff_)) {
				if (!flush()) return false;
			}

			buff_[used_++] = ch;
			return true;
		}

		void discard() {
			used_ = 0;
		}

		constexpr size_t size() const {
			return buffer_size;
		}
	};

	template <typename Char, size_t buffer_size = FORMAT_BUFFER_ALLOC_SIZE>
	class FormatBufferOStream : public FormatBuffer<Char, buffer_size> {
	private:
		using stream_type = typename FormatBuffer<Char, buffer_size>::stream_type;
		stream_type &out_;

		bool write_out(const Char *base, size_t size) final {
			out_.write(base, size);
			return out_.good();
		}

	public:
		FormatBufferOStream(stream_type &out) : FormatBuffer<Char, buffer_size>(), out_(out) { }
		virtual ~FormatBufferOStream() { }
	};

	template <typename Char, size_t buffer_size = FORMAT_BUFFER_ALLOC_SIZE>
	class FormatBufferCStdio : public FormatBuffer<Char, buffer_size> {
	private:
		FILE *out_;
		bool  own_;

		bool write_out(const Char *base, size_t size) final {
			ssize_t towrite = size * sizeof(Char);
			ssize_t written = std::fwrite(base, towrite, 1, out_);
			return written == towrite;
		}

	public:
		FormatBufferCStdio(FILE *out, bool own = false) : FormatBuffer<Char, buffer_size>(), out_(out), own_(own) { }
		virtual ~FormatBufferCStdio() {
			if (own_ && out_) std::fclose(out_);
		}

		FormatBufferCStdio(const FormatBufferCStdio &) = delete;
		FormatBufferCStdio &operator=(const FormatBufferCStdio &) = delete;
	};

	template <typename Char, size_t buffer_size = FORMAT_BUFFER_ALLOC_SIZE>
	class FormatBufferRawIO : public FormatBuffer<Char, buffer_size> {
	private:
		int  out_;
		bool own_;

		bool write_out(const Char *base, size_t size) final {
			ssize_t towrite = size * sizeof(Char);
			ssize_t written = ::write(out_, base, towrite);
			return written == towrite;
		}

	public:
		FormatBufferRawIO(int out, bool own = false) : FormatBuffer<Char, buffer_size>(), out_(out), own_(own) { }
		virtual ~FormatBufferRawIO() {
			if (own_) ::close(out_);
		}
	};
}

#endif // FBUFFER_H
