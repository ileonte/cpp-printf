#ifndef __PRINTF_H_INCLUDED__
#define __PRINTF_H_INCLUDED__

#include <algorithm>
#include <string>
#include <ios>
#include <iostream>
#include <sstream>
#include <cctype>

namespace fancy {
	template<typename VType>
	int printf_impl_print_type(std::ostream &out, std::string::const_iterator begin, std::string::const_iterator end, VType val)
	{
		auto width = -1;
		auto prec  = -1;
		auto fill  = '\0';
		auto ff    = std::ios_base::boolalpha | std::ios_base::fixed;

		if (begin < end && *begin == '-') {
			ff |= std::ios_base::left;
			begin++;
		}
		if (begin < end && (*begin >= '0' && *begin <= '9')) {
			width = 0;
			while (begin < end && (*begin >= '0' && *begin <= '9')) {
				if (!width && *begin == '0')
					fill = '0';
				width = width * 10 + *begin - '0';
				begin++;
			}
		}
		if (begin < end) {
			if (*begin == '.') {
				begin++;
				prec = 0;

				while (begin < end && (*begin >= '0' && *begin <= '9')) {
					prec = prec * 10 + *begin - '0';
					begin++;
				}
			}

			while (begin < end) {
				switch (*begin) {
					case 'e': {
						ff &= ~std::ios_base::floatfield;
						ff |= std::ios_base::scientific;
						break;
					}
					case 'x': {
						ff &= ~std::ios_base::basefield;
						ff |= std::ios_base::hex;
						break;
					}
					case 'o': {
						ff &= ~std::ios_base::basefield;
						ff |= std::ios_base::oct;
						break;
					}
					case 'C': {
						ff |= std::ios_base::uppercase;
						break;
					}
				}
				begin++;
			}
		}


		out.flags(ff);
		out.width(width);
		out.precision(prec);
		if (fill) out.fill(fill);

		auto p = out.tellp();
		out << val;
		return (int)(out.tellp() - p);
	}

	int printf_impl(std::ostream &out, std::string::const_iterator begin, std::string::const_iterator end) {
		out.write(&(*begin), end - begin);
		return end - begin;
	}

	template<typename VType, typename... Ts>
	int printf_impl(std::ostream &out, std::string::const_iterator begin, std::string::const_iterator end, VType first, Ts... vars) {
		auto it = std::find(begin, end, '%');
		if (it >= end)
			return printf_impl(out, begin, end);

		static const char formats[] = "|%";
		auto cmd = std::find_first_of(it + 1, end, formats, formats + sizeof(formats) - 1);
		if (cmd >= end)
			return printf_impl(out, begin, end);

		int ret = printf_impl(out, begin, it);
		switch (*cmd) {
			case '%': {
				out.put('%');
				ret += 1;
				break;
			}
			default: {
				ret += printf_impl_print_type(out, it + 1, cmd, first);
				break;
			}
		}
		ret += printf_impl(out, ++cmd, end, vars...);

		return ret;
	}

	template<typename... Ts>
	int fprintf(std::ostream &out, const std::string &fmt, Ts... vars) {
		out.unsetf(std::ios_base::unitbuf);
		auto ret = printf_impl(out, fmt.begin(), fmt.end(), vars...);
		return ret;
	}

	template<typename... Ts>
	int printf(const std::string &fmt, Ts... vars) {
		return fprintf(std::cout, fmt, vars...);
	}

	template<typename... Ts>
	std::string sprintf(const std::string &fmt, Ts... vars) {
		std::ostringstream out;
		fprintf(out, fmt, vars...);
		return out.str();
	}
}

#endif  /* __PRINTF_H_INCLUDED__ */
