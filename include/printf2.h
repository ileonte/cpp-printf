#ifndef PRINTF2_H
#define PRINTF2_H
#define PRINTF_IMPL_INCLUDED

#include <algorithm>
#include <string>
#include <ios>
#include <iostream>
#include <sstream>
#include <cctype>
#include <type_traits>

#include "fbuffer.h"
#include "printf_utils.h"
#include "printf_inttypes.h"

namespace superfancy {
	template <typename Char, typename VType>
	typename std::enable_if<std::is_integral<VType>::value && std::is_signed<VType>::value, bool>::type
	printf_impl_print_type(FormatBuffer<Char> &buff, const Char *begin, const Char *end, VType val) {
		intformat_opts<Char> opts(begin, end, static_cast<long long>(val));
		return opts.print(buff);
	}

	template <typename Char, typename VType>
	typename std::enable_if<std::is_integral<VType>::value && std::is_unsigned<VType>::value, bool>::type
	printf_impl_print_type(FormatBuffer<Char> &buff, const Char *begin, const Char *end, VType val) {
		intformat_opts<Char> opts(begin, end, static_cast<unsigned long long>(val));
		return opts.print(buff);
	}

	template <typename Char>
	bool printf_impl_print_type(FormatBuffer<Char> &buff, const Char */*begin*/, const Char */*end*/, const Char *val) {
		return buff.write(val, string_length(val));
	}

	template <typename Char>
	bool printf_impl_print_type(FormatBuffer<Char> &buff, const Char */*begin*/, const Char */*end*/, const std::basic_string<Char> &val) {
		return buff.write(val.data(), val.size());
	}

	template <typename Char>
	bool printf_impl(FormatBuffer<Char> &buff, const Char *begin, const Char *end) {
		return buff.write(begin, end - begin);
	}

	template <typename Char, typename VType, typename... Ts>
	int printf_impl(FormatBuffer<Char> &buff, const Char *begin, const Char *end, VType first, Ts... vars) {
		auto it = std::find(begin, end, '%');
		if (it >= end)
			return buff.write(begin, end - begin);

		static constexpr Char formats[] = { '%', '|' };
		static constexpr size_t formats_count = sizeof(formats) / sizeof(Char);
		auto cmd = std::find_first_of(it + 1, end, formats, formats + formats_count);
		if (cmd >= end)
			return buff.write(begin, end - begin);

		if (!buff.write(begin, it - begin)) return false;
		switch (*cmd) {
			case '%': {
				if (!buff.write('%'))
					return false;
				break;
			}
			default: {
				if (!printf_impl_print_type(buff, it + 1, cmd, first))
					return false;
				break;
			}
		}
		return printf_impl(buff, ++cmd, end, vars...);
	}

	template <typename Char, size_t buffer_size = FORMAT_BUFFER_ALLOC_SIZE, typename... Ts>
	bool fprintf(std::basic_ostream<Char, std::char_traits<Char>> &out, const Char *fmt, Ts... vars) {
		FormatBufferOStream<Char, buffer_size> buff(out);
		auto fmt_len = string_length(fmt);
		return printf_impl(buff, fmt, fmt + fmt_len, vars...) && buff.flush();
	}

	template <typename Char, size_t buffer_size = FORMAT_BUFFER_ALLOC_SIZE, typename... Ts>
	bool fprintf(FILE *out, const Char *fmt, Ts... vars) {
		FormatBufferCStdio<Char, buffer_size> buff(out);
		auto fmt_len = string_length(fmt);
		return printf_impl(buff, fmt, fmt + fmt_len, vars...) && buff.flush();
	}

	template <typename Char, size_t buffer_size = FORMAT_BUFFER_ALLOC_SIZE, typename... Ts>
	bool fprintf(int out, const Char *fmt, Ts... vars) {
		FormatBufferRawIO<Char, buffer_size> buff(out);
		auto fmt_len = string_length(fmt);
		return printf_impl(buff, fmt, fmt + fmt_len, vars...) && buff.flush();
	}

	template <typename Char, size_t buffer_size = FORMAT_BUFFER_ALLOC_SIZE, typename... Ts>
	bool printf(const Char *fmt, Ts... vars) {
		return fprintf(std::cout, fmt, vars...);
	}
}

#endif // PRINTF2_H
