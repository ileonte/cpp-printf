#ifndef PRINTF_INTTYPES_H
#define PRINTF_INTTYPES_H

#ifndef PRINTF_IMPL_INCLUDED
#error "You shouldn't be including this file directly"
#endif

#include "fbuffer.h"

namespace superfancy {
	template <typename Char>
	class intformat_opts {
	private:
		bool neg_;
		bool right_;
		bool uc_;
		int width_;
		int cwidth_;
		Char pad_;
		unsigned short base_;
		unsigned long long div_;
		unsigned long long val_;

		void parse(const Char *begin, const Char *end) {
			unsigned long long max = 10000000000000000000ull;
			int cw = 20;

			if (begin < end && *begin == '-') {
				right_ = false;
				begin++;
			}
			if (begin < end && (*begin >= '0' && *begin <= '9')) {
				while (begin < end && (*begin >= '0' && *begin <= '9')) {
					if (!width_ && *begin == '0' && right_)
						pad_ = '0';
					width_ = width_ * 10 + (*begin - '0');
					begin++;
				}
			}
			while (begin < end) {
				switch (*begin) {
					case 'x': {
						base_ = 16;
						max = 0x1000000000000000ull;
						cw = 16;
						break;
					}
					case 'o': {
						base_ = 8;
						max = 01000000000000000000000ull;
						cw = 22;
						break;
					}
					case 'C': {
						uc_ = true;
						break;
					}
				}
				begin++;
			}

			if (val_) {
				while (!(val_ / max)) {
					max /= base_;
					cw -= 1;
				}
				div_ = max;
				cwidth_ = cw;
			} else {
				div_ = 1;
				cwidth_ = 1;
			}

			if (neg_) {
				if (base_ == 10) cwidth_ += 1;
				else neg_ = false;
			}
		}

	public:
		intformat_opts(const Char *begin, const Char *end, long long val)
			: right_(true), uc_(false), width_(0)
			, cwidth_(1), pad_(' '), base_(10), div_(1)
		{
			if (val < 0) {
				neg_ = true;
				val_ = val * -1;
			} else {
				neg_ = false;
				val_ = val;
			}
			parse(begin, end);
		}

		intformat_opts(const Char *begin, const Char *end, unsigned long long val)
			: neg_(false), right_(true), uc_(false), width_(0)
			, cwidth_(1), pad_(' '), base_(10), div_(1), val_(val)
		{
			parse(begin, end);
		}

		bool print(FormatBuffer<Char> &buff) const {
			static constexpr Char lc_letters[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
			static constexpr Char uc_letters[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
			const Char *letters = uc_ ? uc_letters : lc_letters;
			unsigned long long llval = val_;
			unsigned long long lldiv = div_;

			if (base_ == 10 && neg_) {
				if (!buff.write('-')) return false;
			}
			if (right_ && cwidth_ < width_) {
				for (int i = 0; i < width_ - cwidth_; i++) {
					if (!buff.write(pad_)) return false;
				}
			}
			while (lldiv) {
				if (!buff.write(letters[llval / lldiv])) return false;
				llval %= lldiv;
				lldiv /= base_;
			}
			if (!right_ && cwidth_ < width_) {
				for (int i = 0; i < width_ - cwidth_; i++) {
					if (!buff.write(pad_)) return false;
				}
			}

			return true;
		}
	};
}

#endif // PRINTF_INTTYPES_H
