#ifndef PRINTF_UTILS_H
#define PRINTF_UTILS_H

#ifndef PRINTF_IMPL_INCLUDED
#error "You shouldn't be including this file directly"
#endif

#include <cstring>
#include <cstdio>
#include <cwchar>

namespace superfancy {
	size_t string_length(const char *s) {
		return ::strlen(s);
	}

	size_t string_length(const wchar_t *s) {
		return ::wcslen(s);
	}
}

#endif // PRINTF_UTILS_H
