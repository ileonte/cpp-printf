include ../mk/vars.mk

TARGET := test
SRCS   := $(wildcard *.cpp)
SRCS   += $(wildcard *.cc)

DEBUGFLAGS := -ggdb -O3 -DNDEBUG
#DEBUGFLAGS := -ggdb -O0 -D_DEBUG

include ../mk/binary.mk
