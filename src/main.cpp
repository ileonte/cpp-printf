#include <cstdlib>
#include <fstream>
#include <cstring>
#include <climits>
#include <time.h>

#define FORMAT_BUFFER_ALLOC_SIZE 16384

#include <printf.h>
#include <printf2.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

static auto test_string = "This is a rather long string. Why this is so long has to do with std::string small-string-optimization.";
static auto fmtc_string = "This is a test %zd '%s' %zd\n";
static auto fmtC_string = "This is a test %| '%|' %|\n";

int libc_fprintf(size_t count)
{
	FILE *f = ::fopen("ctest.txt", "w+");
	int total = 0;

	for (size_t i = 0; i < count; i++) {
		total += ::fprintf(f, fmtc_string, i, test_string, i, 1.337);
	}

	::fclose(f);
	return total;
}

int fancy_fprintf(size_t count)
{
	std::ofstream f;
	std::string fmt(fmtC_string);

	f.open("cpptest.txt");
	for (size_t i = 0; i < count; i++) {
		fancy::fprintf(f, fmt, i, test_string, 123456);
	}
	f.close();

	return 1;
}

int superfancy_formatbuffer(size_t count)
{
	superfancy::FormatBufferOStream<char> buff(std::cout);
	size_t size = ::strlen(test_string);

	for (size_t i = 0; i < count; i++) {
		buff.write(test_string, size);
		buff.write('\n');
	}
	buff.flush();
	return 1;
}

int superfancy_fprintf_cout(size_t count)
{
	std::ofstream f;
	f.open("cpptest2_cout.txt");
	for (size_t i = 0; i < count; i++) {
		superfancy::fprintf(f, fmtC_string, i, test_string, 123456);
	}
	f.close();
	return 1;
}

int superfancy_fprintf_FILE(size_t count)
{
	FILE *f = std::fopen("cpptest2_FILE.txt", "w+");
	for (size_t i = 0; i < count; i++) {
		superfancy::fprintf(f, fmtC_string, i, test_string, 123456);
	}
	std::fclose(f);
	return 1;
}

int superfancy_fprintf_RAW(size_t count)
{
	int f = ::open("cpptest2_RAW.txt", O_RDWR | O_TRUNC | O_CREAT, 0644);
	for (size_t i = 0; i < count; i++) {
		superfancy::fprintf(f, fmtC_string, i, test_string, 123456);
	}
	::close(f);
	return 1;
}

int libc_fprintf2(size_t count)
{
	FILE *f = ::fopen("ctest2.txt", "w+");
	for (size_t i = 0; i < count; i++) {
		::fprintf(f, fmtc_string, i, test_string, 123456);
	}
	::fclose(f);
	return 1;
}

#define RUNTEST(stmt) { \
	::printf("%30s : ", #stmt); fflush(stdout); \
	clock_t start = clock(); \
	auto ret = stmt; \
	clock_t end = clock(); \
	::printf("%12zd (%d)\n", static_cast<size_t>(end - start), ret); \
}

constexpr size_t count = 1000000;

int main()
{
//	RUNTEST(libc_fprintf(count));
	RUNTEST(libc_fprintf2(count));

//	RUNTEST(superfancy_formatbuffer(count));
	RUNTEST(fancy_fprintf(count));
	RUNTEST(superfancy_fprintf_cout(count));
//	RUNTEST(superfancy_fprintf_FILE(count));
//	RUNTEST(superfancy_fprintf_RAW(count));

	return 0;
}
